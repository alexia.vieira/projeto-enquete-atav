from django.contrib import admin
from .models import Pergunta,Opcao,Comentario


admin.site.register(Pergunta)
admin.site.register(Opcao)
admin.site.register(Comentario)