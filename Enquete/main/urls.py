from django.urls import path
from . import views
app_name = 'main'
urlpatterns = [
    path('',views.IndexView.as_view(), name='index'),

    path('enquete/<int:pk>', views.DetalhesView.as_view(), name='detalhes'),

    path('enquete/<int:pk>/resultado', views.ResultadoView.as_view(), name='resultado'),

    path('enquete/<int:id_enquete>/votacao', views.Votacao, name='votacao'),

    path('enquete/<int:pk>/comentario', views.ComentarioView.as_view(), name='comentario'),


    ]