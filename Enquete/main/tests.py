import datetime
from django.test import TestCase
from django.utils import timezone
from .models import Pergunta

class PerguntaTeste(TestCase):
    def test_publicada_recentemente_com_pergunta_no_futuro(self):
        """
        O metodo publicado_recentemente precisa retornar FALSE quando se tratar de perguntas com data de publicacao no futuro
        """
        data = timezone.now()+datetime.timedelta(seconds=1)
        pergunta_futura = Pergunta(data_publicacao = data)
        self.assertIs(pergunta_futura.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_anterior_a_24h_no_passado(self):
        """
        O metodo publicado_recentemente deve retornar false quando se tratar de uma data de publicação anterior a 24h no passado
        """
        data = timezone.now()-datetime.timedelta(days=1, seconds=1)
        pergunta_passado = Pergunta(data_publicacao = data)
        self.assertIs(pergunta_passado.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_nas_ultimas_24hrs(self):
        """
            O metodo publicada_recentemente retorna true quando se trata de uma publicação das ultimas 24hrs
        """
        data = timezone.now() - datetime.timedelta(hours=23,minutes=59,seconds=59)
        pergunta_ok = Pergunta(data_publicacao = data)
        self.assertIs(pergunta_ok.publicada_recentemente(), True)

def criar_pergunta(texto, dias):
    """
    funcao para criacao de uma pergunta com texto e uma variacao de dias
    """
    data = timezone.now() + datetime.timedelta(days=dias)
    return Pergunta.objects.create(texto=texto, data_publicacao=data)
    
